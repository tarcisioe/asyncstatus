from asyncio import run

from asyncstatus import run_blocks
from asyncstatus.cleese import cleese_blocks


run(run_blocks([cleese_blocks()]))
