'''Module for creating a status bar with MPD information based on Cleese.'''
from asyncio import sleep, Queue
from contextlib import asynccontextmanager
from dataclasses import dataclass, field
from typing import (
    Any, AsyncGenerator, List, NamedTuple, NewType, Optional, Union,
)

from cleese import (
    get_default_client, playpause, playlist,
    ConnectionFailedError, CleeseClient, Subsystem, State, Status, Song,
    NoCurrentSongError,
)
from proggy import LogicalProgressBar, BarInfo

from . import any_yield, click_filter, Click, Block, BlockLike
from .notify import notify


class IdleEvent(NamedTuple):
    '''Event representing an update from `CleeseClient.idle`'''
    subsystems: List[Subsystem]


class DisconnectedEvent:
    '''Event that happens in case of disconnection.'''


class ReconnectedEvent:
    '''Event that happens when reconnection is succesful.'''


class TickEvent:
    '''Event that happens for every tick.'''


Event = Union[IdleEvent, TickEvent]


@dataclass
class MPDState:
    '''Current state of the MPD server.'''
    current_song: Optional[Song]
    status: Status


DisconnectedCounter = NewType('DisconnectedCounter', int)


CleeseState = Union[MPDState, DisconnectedCounter]


async def maybe_current_song(client: CleeseClient) -> Optional[Song]:
    '''Get the current song, if any.'''
    try:
        return await client.current_song()
    except NoCurrentSongError:
        return None


async def get_full_state(client: CleeseClient) -> MPDState:
    '''Get the full state of the MPD server.'''
    current_song = await maybe_current_song(client)
    status = await client.status()

    return MPDState(
        current_song,
        status,
    )


@dataclass
class CleeseStatus:
    '''Status bar generator using Cleese.'''
    current_state: Optional[CleeseState]
    client: CleeseClient
    events: Queue = field(default_factory=Queue)
    idle_loop: Any = None
    ticker_loop: Any = None

    @staticmethod
    @asynccontextmanager
    async def make():
        '''Context manager to get a connected `CleeseState`.'''
        async with get_default_client() as client:
            c = CleeseStatus(
                await get_full_state(client),
                client,
            )

            yield c

    async def updates(self) -> AsyncGenerator[Optional[CleeseState], None]:
        '''Method that yields the state whenever an update happens.

        Yields:
            Either an MPDState or a DisconnectedCounter with the time remaining
            for reconnection to be attempted.
        '''
        yield self.current_state
        async for _, event in any_yield([self._ticker(), self._idle_loop()]):
            await self._handle(event)
            yield self.current_state

    async def force_reconnect(self):
        '''Force reconnection with MPD.'''
        try:
            await self.client.reconnect()
        except ConnectionFailedError:
            return

        self.current_state = await get_full_state(self.client)

    async def _update_status(self):
        if isinstance(self.current_state, MPDState):
            self.current_state.status = await self.client.status()

    async def _update_current_song(self):
        if isinstance(self.current_state, MPDState):
            self.current_state.current_song = (
                await maybe_current_song(self.client)
            )

    async def _handle(self, event):
        handler = self.EVENT_HANDLERS.get(type(event))

        if handler is not None:
            await handler(self, event)

    async def _handle_idle(self, event):
        for subsystem in event.subsystems:
            handler = self.SUBSYSTEM_HANDLERS.get(subsystem)

            if handler is not None:
                await handler(self)

    async def _handle_disconnect(self, _):
        pass

    async def _handle_reconnect(self, _):
        self.current_state = await get_full_state(self.client)

    async def _handle_tick(self, _):
        await self._update_status()

    async def _handle_player(self):
        await self._update_status()
        await self._update_current_song()

    async def _handle_mixer(self):
        await self._update_status()

    async def _reconnect_idle(self):
        time = 1
        self.current_state = DisconnectedCounter(time)

        while True:
            try:
                await self.client.reconnect()
            except ConnectionFailedError:
                for i in range(time):
                    if isinstance(self.current_state, MPDState):
                        yield ReconnectedEvent()
                        return

                    self.current_state = DisconnectedCounter(time - i)

                    yield DisconnectedEvent()
                    await sleep(1)

                time = min(time * 2, 16)
            else:
                yield ReconnectedEvent()

    async def _idle_loop(self):
        while True:
            try:
                subsystems = await self.client.idle(
                    Subsystem.PLAYER,
                    Subsystem.MIXER
                )
            except ConnectionFailedError:
                async for event in self._reconnect_idle():
                    yield event
            else:
                yield IdleEvent(subsystems)

    async def _ticker(self):
        while True:
            await sleep(1)
            if (isinstance(self.current_state, MPDState) and
                    self.current_state.status.state == State.PLAY):
                yield TickEvent()

    EVENT_HANDLERS = {
        IdleEvent: _handle_idle,
        TickEvent: _handle_tick,
        DisconnectedEvent: _handle_disconnect,
        ReconnectedEvent: _handle_reconnect,
    }

    SUBSYSTEM_HANDLERS = {
        Subsystem.PLAYER: _handle_player,
        Subsystem.MIXER: _handle_mixer,
    }


def fmt_minutes(seconds: int) -> str:
    '''Format seconds as minutes and seconds.

    Args:
        seconds (int): The number of seconds.

    Returns:
        str: The value formatted as `mm:ss`.
    '''
    m, s = divmod(seconds, 60)
    return f'{m}:{s:02}'


STATE_SYMBOLS = {
    State.PLAY: '',
    State.STOP: '',
    State.PAUSE: '',
}


async def seek_handler(click: Click, client: CleeseClient, total: int):
    '''Takes a click and seeks the song based on its position.'''
    relative = click.relative_x/click.width
    where = int(total*relative)
    await client.seek_cur_abs(where)


def make_cleese_blocks(
    state: CleeseState,
    status: CleeseStatus
) -> List[BlockLike]:
    '''Make blocks based on Cleese's current state.'''
    if not isinstance(state, MPDState):
        return [Block(
            f'Disconnected, will reconnect in {state}.',
            name='disconnected',
            handler=click_filter({
                1: lambda _: status.force_reconnect()
            })
        )]

    if state.status.state == State.STOP:
        return []

    if state.current_song is None:
        return []

    current_song: Song = state.current_song

    elapsed = fmt_minutes(int(state.status.elapsed))
    time = fmt_minutes(current_song.time)
    track = state.status.song + 1
    total = state.status.playlistlength
    volume = state.status.volume if state.status.volume is not None else 0
    volume_txt = (f'{state.status.volume}'
                  if state.status.volume is not None
                  else 'n/a')

    async def notify_playlist(_):
        playlist_text = await playlist(status.client)
        lines = playlist_text.splitlines()
        i = next(i for i, l in enumerate(lines) if l.startswith('->'))

        await notify('\n'.join(lines[i-5:i+6]), 2000)

    return [
        Block(
            f'{current_song.artist}/{current_song.album}/{current_song.title}',
            name='song',
            handler=click_filter({
                1: notify_playlist,
            }),
        ),
        f'{track}/{total}',
        Block(
            '',
            name='prev',
            separator=False,
            handler=click_filter({
                1: lambda _: status.client.previous(),
            }),
        ),
        Block(
            STATE_SYMBOLS[state.status.state],
            name='play',
            align='center',
            min_width='',
            separator=False,
            handler=click_filter({
                1: lambda _: playpause(status.client),
            }),
        ),
        Block(
            '',
            name='next',
            handler=click_filter({
                1: lambda _: status.client.next(),
            }),
        ),
        Block(
            f'{volume_txt}%',
            name='volume',
            align='right',
            min_width='100%',
            handler=click_filter({
                4: lambda _: status.client.setvol(volume + 1),
                5: lambda _: status.client.setvol(volume - 1),
            }),
        ),
        Block(
            f'{elapsed}/{time}',
            name='time',
            handler=click_filter({
                4: lambda _: status.client.seek_cur_rel(+1),
                5: lambda _: status.client.seek_cur_rel(-1),
            }),
        ),
        Block(
            LogicalProgressBar(BarInfo(
                size=30,
                total=current_song.time,
                characters='  ▏▎▍▌▋▊▉█',
                progress=min(current_song.time, state.status.elapsed),
            )).render(),
            name='bar',
            handler=click_filter({
                1: lambda c: seek_handler(c, status.client, current_song.time),
                4: lambda _: status.client.seek_cur_rel(+1),
                5: lambda _: status.client.seek_cur_rel(-1),
            }),
        )
    ]


async def cleese_blocks():
    '''Generator that yields blocks using CleeseStatus.'''
    while True:
        try:
            async with CleeseStatus.make() as cleese_status:
                async for state in cleese_status.updates():
                    yield make_cleese_blocks(state, cleese_status)
        except ConnectionFailedError:
            await sleep(5)
