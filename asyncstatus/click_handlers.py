'''Module for functions to help creating click handlers.'''
from typing import Dict

from .types import Click, Handler


def click_filter(handlers: Dict[int, Handler]) -> Handler:
    '''Create a handler that maps different buttons to different handlers.'''
    async def handler(click: Click):
        if click.button in handlers:
            await handlers[click.button](click)
    return handler


__all__ = [
    'click_filter',
]
