'''Async generator utilities module.'''
from asyncio import create_task, wait, Future, FIRST_COMPLETED  # pylint: disable=unused-import # noqa
from typing import (
    Any, AsyncGenerator, Dict, Iterable, Tuple
)


AnyGen = AsyncGenerator[Any, None]


async def any_yield(
    generators: Iterable[AnyGen]
) -> AsyncGenerator[Tuple[AnyGen, Any], None]:
    '''Create an async gen. that yields whenever an async gen. passed to it yields.

    Args:
        generators: An iterable of `AsyncGenerator`s.

    Returs:
        An async generators that streams the yields of
    '''
    GenDict = Dict['Future[Any]', Any]

    tasks: GenDict = {
        create_task(gen.__anext__()): gen for gen in generators
    }

    while tasks:
        done, _ = await wait(
            tasks,
            return_when=FIRST_COMPLETED
        )

        for d in done:
            gen = tasks[d]
            del tasks[d]

            try:
                result = d.result()
            except StopAsyncIteration:
                pass
            else:
                tasks[create_task(gen.__anext__())] = gen
                yield gen, result
