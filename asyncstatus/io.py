'''File IO module.'''
from asyncio import get_running_loop, StreamReader, StreamReaderProtocol

from typing import IO


async def build_reader(f: IO) -> StreamReader:
    '''Build an `asyncio.StreamReader` from an `IO` (file-like) object.'''
    loop = get_running_loop()

    reader = StreamReader()
    reader_protocol = StreamReaderProtocol(reader)

    await loop.connect_read_pipe(lambda: reader_protocol, f)

    return reader
