from subprocess import run


def do_notify(text: str, timeout: int):
    run(['notify-send', '-t', f'{timeout}', ' ', text], check=False)
