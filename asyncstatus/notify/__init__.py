try:
    from .gi_notify import do_notify
except ImportError:
    from .notify_send import do_notify


async def notify(text: str, timeout: int):
    do_notify(text, timeout)
