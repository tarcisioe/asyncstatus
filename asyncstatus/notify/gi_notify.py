import gi
gi.require_version('Notify', '0.7')

from contextlib import contextmanager

from gi.repository import Notify


Notify.init('asyncstatus')


def do_notify(text: str, timeout: int):
    notification = Notify.Notification.new(' ', text)
    notification.set_timeout(timeout)
    notification.show()
