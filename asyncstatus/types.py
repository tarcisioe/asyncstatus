'''Basic types used for the status bar.'''
from typing import (
    Any, AsyncGenerator, Awaitable, Callable, Iterable, List, NamedTuple,
    Optional, Union, TYPE_CHECKING
)


class Click(NamedTuple):
    '''Data provided when the user clicks a block.'''
    x: int
    y: int
    button: int
    relative_x: int
    relative_y: int
    width: int
    height: int
    modifiers: List[str]
    name: Optional[str] = None
    instance: Optional[str] = None


Handler = Callable[[Click], Awaitable[None]]


class Block(NamedTuple):
    '''Information of a block to serialize as json and send to the bar.'''
    full_text: str
    short_text: Optional[str] = None
    color: Optional[str] = None
    background: Optional[str] = None
    border: Optional[str] = None
    min_width: Optional[Union[int, str]] = None
    align: Optional[str] = None
    name: Optional[str] = None
    instance: Optional[str] = None
    urgent: Optional[bool] = None
    separator: Optional[bool] = None
    separator_block_width: Optional[int] = None
    markup: Optional[str] = None
    handler: Optional[Handler] = None


if TYPE_CHECKING:
    BlockLike = Union[Block, Iterable[Any], str, int, None]
else:
    BlockLike = Union[Block, Iterable['BlockLike'], str, int, None]


BlockSource = AsyncGenerator[BlockLike, None]


__all__ = [
    'Block',
    'BlockLike',
    'Click',
    'Handler',
]
