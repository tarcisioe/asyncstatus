'''Module implementing the bar protocol.'''
import sys
import json

from typing import AsyncGenerator, List, Iterable

from .asyncgen import any_yield
from .block import make_blocks, to_dict
from .io import build_reader
from .types import Block, BlockSource, Click


BlockIterSource = AsyncGenerator[Iterable[Block], None]
ClickSource = AsyncGenerator[Click, None]


async def get_clicks() -> ClickSource:
    '''Get a stream of `Click` objects from stdin.'''
    reader = await build_reader(sys.stdin)

    await reader.readline()
    while True:
        string = (await reader.readline()).decode('utf8').strip().lstrip(',')
        values = json.loads(string)
        yield Click(**values)


async def read_sources(l: Iterable[BlockSource]) -> BlockIterSource:
    '''Transform an iterable of `BlockSource`s into a source of Block iterables.

    Args:
        l: An iterable of `BlockSource`.

    Returns:
        A generator that yields the list of the latest values provided by each
        `BlockSource`.
    '''
    values = {gen: await gen.__anext__() for gen in l}
    yield make_blocks(values.values())

    async for generator, value in any_yield(l):
        values[generator] = value

        yield make_blocks(values.values())


async def handle_click(blocks: Iterable[Block], click: Click):
    '''Match a click to a block in an iterable and call its handler.'''
    if click.name is None:
        return

    for block in blocks:
        if block.handler is None:
            continue

        if (block.name, block.instance) == (click.name, click.instance):
            await block.handler(click)
            return


async def handle_events(
    block_source: BlockSource,
    click_source: ClickSource,
) -> BlockIterSource:
    '''Match clicks and blocks, call the click handler, and forward the blocks.

    Args:
        block_source: A source of `BlockLike`s.
        click_source: A source of `Click`s.

    Returns:
        An async generator that forwards the blocks.
    '''
    blocks: List[Block] = []
    async for generator, value in any_yield((block_source, click_source)):
        if generator is block_source:
            blocks = value
            yield blocks
        if generator is click_source:
            await handle_click(blocks, value)


async def run_blocks(l: List[BlockSource]):
    '''Run a block-based status generator based on a list of block generators.

    Args:
        l: A list of async generators that yield `BlockLike`s.
    '''
    print('{ "version": 1, "click_events": true }')
    print('[')
    async for x in handle_events(read_sources(l), get_clicks()):
        out = json.dumps([to_dict(b) for b in x])
        print(f'{out},', flush=True)
