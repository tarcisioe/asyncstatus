'''Block handling module.'''
from typing import (
    Any, Dict, Iterable, List
)

from .types import Block, BlockLike


def to_dict(block: Block) -> Dict[str, Any]:
    '''Transform a block into a dict, minus the click handler.

    Args:
        block: The block to transform.

    Returns:
        A json-serializable dict representing the block.
    '''
    return {
        k: v
        for k, v in block._asdict().items()
        if k != 'handler' and v is not None
    }


def make_blocks(l: Iterable[BlockLike]) -> List[Block]:
    '''Flatten and wrap a list of `BlockLike`s into blocks.

    Args:
        l: An iterable of `BlockLike`s.

    Returs:
        A flattened list of blocks, wrapping other types as necessary.
    '''
    done = []

    for x in l:
        if isinstance(x, Block):
            done.append(x)
        elif isinstance(x, str):
            done.append(Block(x))
        elif isinstance(x, Iterable):
            done.extend(make_blocks(x))
        elif x is not None:
            done.append(Block(str(x)))

    return done
